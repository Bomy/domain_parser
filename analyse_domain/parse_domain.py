#!/usr/bin/env python
# coding=utf-8
"""
域名解析
"""
import logging
import re
import json
import requests
from multiprocessing import Pool, freeze_support, Manager

request_method_key = "request_method"
# 域名文件
domain_file = "./domains"
# 结果文件
result_file = "./domain_detail"
content_key = "content"
exits_value = "exit"


def request_data_common(url):
    """
    请求网络书
    :param url:请求的url
    :return: html内容
    """
    return requests.get(url).content


# 针对不同域名备案查询网址的配置
urls_cfg = [{'http://www.beianbeian.com/s-0/{}.html': {request_method_key: request_data_common, content_key: {
    "主办单位名称": re.compile('<tbody id="table_tr">.*?</td>.*?<td>(.*?)</td>.*', flags=re.S)}}},
            {'http://icp.chinaz.com/{}': {request_method_key: request_data_common,
                                          content_key: {"主办单位名称": re.compile("主办单位名称</span><p>(.*?)<", flags=re.S)}}}]


def start_process_pool(domain_list, lock, process_num=3):
    """
    启动进程池
    :param domain_list: 域名存储集合
    :param lock: 全局锁，避免不同进程读取到同一个域名
    :param process_num: 处理的进程数，在解析域名过程中，时间主要花费在网络解析上，所以需要开多个进程进行网络请求
    :return:
    """
    pool = Pool(process_num + 1)
    pool.apply_async(func=put_domain, args=(domain_list,))
    for i in range(0, process_num):
        pool.apply_async(func=deal_domain, args=(domain_list, lock))
    pool.close()
    pool.join()


def deal_domain(domain_list, lock):
    """
    读取域名集合中的域名进行解析
    :param domain_list: 域名集合
    :param lock: 全局锁
    :return:
    """
    with open(result_file, "a+") as res_file:
        while True:
            try:
                lock.acquire()
                # 获取域名集合中的第一个域名
                domain = domain_list.pop(0)
                lock.release()
                # 如果解析到退出标志，则退出当前进程，并且将退出标志写入域名集合中，让其他进程收到退出标志
                if domain == exits_value:
                    domain_list.append(exits_value)
                    break
                for ele in urls_cfg:
                    res = dict()
                    for _url, _parser in ele.items():
                        url = _url.format(domain)
                        # 不同的网址可能存在不同的请求方式，比如需要携带请求头等，这里读取的配中包含用户可以自定义配置请求方式
                        html_str = _parser[request_method_key](url)
                        # 不同网址对应的html中解析对应字段的正则表达式不同，所以需要在配置中配置对应的正则表达式
                        for title, reg in _parser[content_key].items():
                            value = reg.findall(html_str)[0]
                            if not value:
                                continue
                            res[title] = value
                    if not res:
                        continue
                    logging.info(res)
                    lock.acquire()
                    # 将查询结果写入结果文件
                    res_file.write(domain)
                    for item in res.items()[:-1]:
                        res_file.write(":".join(item) + ",")
                    res_file.write(":".join(res.items()[-1]) + "\n")
                    lock.release()
                    break
            except IndexError as index_err:
                logging.warn(index_err)
            except Exception as e:
                logging.exception(e)


def put_domain(domain_list):
    """
    将域名文件中的域名放入域名集合中
    :param domain_list:域名集合
    :return:
    """
    with open(domain_file) as domains:
        for line in domains:
            line = line.strip()
            if not line:
                continue
            while True:
                try:
                    domain_list.append(line)
                    break
                except Exception as e:
                    logging.warning(e)
        # 因为写入域名是一个单独的进程，为了让其他进程知道域名已经写入完成，这里写入一个exits_value，表示域名已处理完成，可以退出
    domain_list.append(exits_value)
    print exits_value


def main():
    """
    主函数
    :return:
    """
    try:
        freeze_support()
        manager = Manager()
        domain_lst = manager.list()
        lck = manager.Lock()
        start_process_pool(domain_lst, lck)
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    main()
