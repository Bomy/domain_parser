#!/usr/bin/env python
# coding=utf-8

from unittest import TestCase
import requests
from multiprocessing import Pool, freeze_support, Manager
from analyse_domain import parse_domain


class TestParseDomain(TestCase):
    def setUp(self):
        class Html:
            content = """
                <div class="layui-container main_box">
                <!--不带验证码查询-->
        <blockquote class="layui-elem-quote layui-quote-nm">
            <div class="box_div">
                <form class="layui-form one_form">
                    <div style="text-align: left;width: 150px;position:absolute;">
                        <select class="layui-form-select"  name="keytype" lay-filter="keytype">
                            <option value="0" selected >网站域名</option>
                            <option value="1"  >备案/许可证号</option>
                            <option value="2"  >主办单位/个人名称</option>
                            <option value="3"  >网站名称</option>
                            <option value="4"  >负责人</option>
                            <!--<option value="5"  >网站备案/许可证号</option>-->
                            <option value="6"  >网站IP地址</option>
                        </select>
                    </div>
                    <div class="layui-form-item  input_0">
                        <input value="www.sina.com"  type="text" name="q"  autocomplete="off" class="layui-input search_input" required  lay-verify="required">
                        <button onclick="return_url(this.form)" type="button"  class="layui-btn layui-btn-normal submit_btn search_btn">查询</button>
                    </div>
                </form>
            </div>
        </blockquote>
       
        <!--查询结果-->
        <table class="layui-table res_table" style="margin-bottom: 20px">
            <colgroup>
                <col width="70">
                <col width="170">
                <col width="120">
                <col width="220">
                <col width="170">
                <col width="190">
            </colgroup>
            <thead>
                <tr>
                    <th>序号</th>
                    <th>单位名称</th>
                    <th>单位性质</th>
                    <th>网站备案/许可证号</th>
                    <th>网站名称</th>
                    <th>网站首页网址</th>
                    <th>审核时间</th>
                    <th>详细</th>
                </tr>
            </thead>
            <tbody id="table_tr">
                                        <tr>
                            <td>1</td>
                            <td>北京新浪互联信息服务有限公司</td>
                            <td>企业</td>
                            <td><a href='/beianxinxi/feb1ca258e4ea23cffed0659e71f6272.html'>京ICP证000007-2</a>                                                                <a href="/search-1/京ICP证000007.html">[反查]</a>
                                                            </td>
                            <td>新浪网</td>
                            <td><a href='http://www.sina.com' target='_blank'> www.sina.com</a><br></td>
                            <td>2019-02-19</td>
                            <td>
                                                                    <a href='/beianxinxi/feb1ca258e4ea23cffed0659e71f6272.html'>详细信息</a>
                                                            </td>
                            <!--feb1ca258e4ea23cffed0659e71f6272-->
                        </tr>
                

            </tbody>
        </table>
            """

        def get(url):
            return Html()

        requests.get = get

    def test_put_domain(self):
        freeze_support()
        manager = Manager()
        domain_lst = manager.list()
        lck = manager.Lock()
        parse_domain.push_domain(domain_lst, lck)
        self.assertEqual(len(domain_lst), 4)

    def test_deal_domain(self):
        freeze_support()
        manager = Manager()
        domain_lst = manager.list()
        lck = manager.Lock()
        parse_domain.push_domain(domain_lst, lck)
        parse_domain.deal_domain(domain_lst, lck)

    def test_main(self):
        parse_domain.main()
